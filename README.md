# OpenMailData

https://openmaildata.com

> Opening mail data for everyone!


## Install

1. Clone this repository.

2. Install Python (if not already installed) and the dependancies `cherrypy` & `mail-parser` using `pip`.

3. Launch the server by typing `python server.py`.

4. Open `http://localhost:8080`.


