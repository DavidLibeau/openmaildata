# Search IMAP

> Here is a reformated extract of the [RFC 3501 standard for IMAPv4](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.4) (March 2003) about the search command.

## `SEARCH` Command

### Arguments

OPTIONAL `CHARSET` specification searching criteria (one or more)

### Responses

REQUIRED untagged response: `SEARCH`

### Result

- `OK` : search completed
- `NO` : search error: can't search that `CHARSET` or criteria
- `BAD` : command unknown or arguments invalid

### Details

The SEARCH command searches the mailbox for messages that match
the given searching criteria.  Searching criteria consist of one
or more search keys.  The untagged SEARCH response from the server
contains a listing of message sequence numbers corresponding to
those messages that match the searching criteria.

When multiple keys are specified, the result is the intersection
(AND function) of all the messages that match those keys.  For
example, the criteria `DELETED FROM "SMITH" SINCE 1-Feb-1994` refers
to all deleted messages from Smith that were placed in the mailbox
since February 1, 1994.  A search key can also be a parenthesized
list of one or more search keys (e.g., for use with the OR and NOT
keys).

Server implementations MAY exclude `MIME-IMB` body parts with
terminal content media types other than TEXT and MESSAGE from
consideration in SEARCH matching.

The OPTIONAL `CHARSET` specification consists of the word
"CHARSET" followed by a registered `CHARSET`.  It indicates the
`CHARSET` of the strings that appear in the search criteria.
`MIME-IMB` content transfer encodings, and `MIME-HDRS` strings in
`RFC-2822`/`MIME-IMB` headers, MUST be decoded before comparing
text in a `CHARSET` other than US-ASCII.  US-ASCII MUST be
supported; other `CHARSET`s MAY be supported.

If the server does not support the specified `CHARSET`, it MUST
return a tagged NO response (not a BAD).  This response SHOULD
contain the BADCHARSET response code, which MAY list the
`CHARSET`s supported by the server.

In all search keys that use strings, a message matches the key if
the string is a substring of the field.  The matching is
case-insensitive.

The defined search keys are as follows.  Refer to the Formal
Syntax section for the precise syntactic definitions of the
arguments.

## searching criteria (one or more)

### `<sequence set>`

Messages with message sequence numbers corresponding to the specified message sequence number set.

### `ALL`

All messages in the mailbox; the default initial key for ANDing.

### `ANSWERED`

Messages with the \Answered flag set.

### `BCC <string>`

Messages that contain the specified string in the envelope structure's BCC field.

### `BEFORE <date>`

Messages whose internal date (disregarding time and timezone) is earlier than the specified date.

### `BODY <string>`

Messages that contain the specified string in the body of the message.

### `CC <string>`

Messages that contain the specified string in the envelope structure's CC field.

### `DELETED`

Messages with the \Deleted flag set.

### `DRAFT`

Messages with the \Draft flag set.

### `FLAGGED`

Messages with the \Flagged flag set.

### `FROM <string>`

Messages that contain the specified string in the envelope structure's FROM field.

### `HEADER <field-name> <string>`

Messages that have a header with the specified field-name (as defined in `RFC-2822`) and that contains the specified string in the text of the header (what comes after the colon).  If the string to search is zero-length, this matches all messages that have a header line with the specified field-name regardless of the contents.

### `KEYWORD <flag>`

Messages with the specified keyword flag set.

### `LARGER <n>`

Messages with an `RFC-2822` size larger than the specified number of octets.

### `NEW`

Messages that have the \Recent flag set but not the \Seen flag. This is functionally equivalent to "(RECENT UNSEEN)".

### `NOT <search-key>`

Messages that do not match the specified search key.

### `OLD`

Messages that do not have the \Recent flag set.  This is functionally equivalent to "NOT RECENT" (as opposed to "NOT NEW").

### `ON <date>`

Messages whose internal date (disregarding time and timezone) is within the specified date.

### `OR <search-key1> <search-key2>`

Messages that match either search key.

### `RECENT`

Messages that have the \Recent flag set.

### `SEEN`

Messages that have the \Seen flag set.

### `SENTBEFORE <date>`

Messages whose `RFC-2822` Date: header (disregarding time and timezone) is earlier than the specified date.

### `SENTON <date>`

Messages whose `RFC-2822` Date: header (disregarding time and timezone) is within the specified date.

### `SENTSINCE <date>`

Messages whose `RFC-2822` Date: header (disregarding time and timezone) is within or later than the specified date.

### `SINCE <date>`

Messages whose internal date (disregarding time and timezone) is within or later than the specified date.

### `SMALLER <n>`

Messages with an `RFC-2822` size smaller than the specified number of octets.

### `SUBJECT <string>`

Messages that contain the specified string in the envelope structure's SUBJECT field.

### `TEXT <string>`

Messages that contain the specified string in the header or body of the message.

### `TO <string>`

Messages that contain the specified string in the envelope structure's TO field.

### `UID <sequence set>`

Messages with unique identifiers corresponding to the specified unique identifier set.  Sequence set ranges are permitted.

### `UNANSWERED`

Messages that do not have the \Answered flag set.

### `UNDELETED`

Messages that do not have the \Deleted flag set.

### `UNDRAFT`

Messages that do not have the \Draft flag set.

### `UNFLAGGED`

Messages that do not have the \Flagged flag set.

### `UNKEYWORD <flag>`

Messages that do not have the specified keyword flag set.

### `UNSEEN`

Messages that do not have the \Seen flag set.


## Examples

- `FLAGGED SINCE 1-Feb-1994 NOT FROM "Smith"`
- `TEXT "string not in mailbox"`
- `CHARSET UTF-8 TEXT {6}`
- `SINCE 1-Aug-2004`
- `OR (FROM yahoo.com) (FROM hotmail.com)`
