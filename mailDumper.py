import json
import mailparser
from imaplib import IMAP4_SSL

def checkLogin(login, passwd, server=''):
    M = connect(login, passwd, server)
    print(type(M))
    print(M)
    if type(M) is dict:
        return M
    if type(M) is IMAP4_SSL:
        return {'status': 'ok'}

    
def connect(login, passwd, server=''):
    if server=='':
        server = login.split("@")[1]
    print('Connecting to ' + server)
    try:
        M = IMAP4_SSL(server, timeout=10)
    except:
        print('timeout')
        return {'status':'ko', 'error':'Server unreachable'}
    try:
        noop = M.noop();
        print(noop);
    except:
        return {'status':'ko', 'error':'Server unreachable'}
    if noop[0]!='OK':
        return {'status':'ko', 'error':'Server unreachable', 'msg':noop[1]}
    try:
        login = M.login(login, passwd)
    except:
        return {'status':'ko', 'error':'Could not login'}
    if login[0]!='OK':
        print(login);
        return {'status':'ko', 'error':'Wrong login', 'msg':login[1]}
    return M
    

def dump(login, passwd, server='', search='ALL'):
    M = connect(login, passwd, server)
    if type(M) is dict:
        return M
    if type(M) is IMAP4_SSL:
        M.select()
        typ, data = M.search(None, search)
        response = []
        for num in data[0].split():
            #print('----------------------------')
            typ, data = M.fetch(num, '(RFC822)')
            mail = mailparser.parse_from_bytes(data[0][1])
            #print('--- From:')
            #print(mail.from_json)
            #print('--- To:')
            #print(mail.to_json)
            #print('--- Date:')
            #print(mail.date_json)
            #print('--- Subject:')
            #print(mail.subject_json)
            #print('--- Body:')
            #print(mail.body_json)
            response.append({'from':json.loads(mail.from_json), 'to':json.loads(mail.to_json), 'date':json.loads(mail.date_json), 'subject':json.loads(mail.subject_json), 'body':json.loads(mail.body_json)})
        M.close()
        M.logout()
        response = json.dumps(response)
        return response
