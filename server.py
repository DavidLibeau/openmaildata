import os, os.path
import json

import cherrypy

import mailDumper

class ApiOpenMailData(object):
    @cherrypy.expose
    def index(self):
        return open('index.html')


@cherrypy.expose
class DumpMailWebService(object):

    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_out()
    def GET(self):
        return {'error': 'You should do a POST request'}

    def POST(self, login='', passwd='', server='', search=''):
        if login=='':
            body = cherrypy.request.body.read()
            if body:
                data = json.loads(body)
                response = json.dumps(data)
                login = data['login']
                passwd = data['passwd']
                server = data['server']
                search = data['search']
                
        response = mailDumper.dump(login, passwd, server, search)
        
        if type(response) is dict:
            response = json.dumps(response)
        if type(response) is str:
            response = bytes(response, 'utf-8')
        print(response)
        return response

@cherrypy.expose
class CheckLoginWebService(object):

    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_out()
    def GET(self):
        return {'error': 'You should do a POST request'}

    def POST(self, login='', passwd='', server=''):
        if login=='':
            body = cherrypy.request.body.read()
            if body:
                data = json.loads(body)
                response = json.dumps(data)
                login = data['login']
                passwd = data['passwd']
                server = data['server']
                
        if login=='demo@example.org':
            response = {'status': 'ok'}
        elif login=='demo-error@example.org':
            response = {'status':'ko', 'error':'Server unreachable'}
        else:
            response = mailDumper.checkLogin(login, passwd, server)
        
        if type(response) is dict:
            response = json.dumps(response)
        if type(response) is str:
            response = bytes(response, 'utf-8')
        print(response)
        return response


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/dumpMail': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'application/json')],
        },
        '/checkLogin': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'application/json')],
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    webapp = ApiOpenMailData()
    webapp.dumpMail = DumpMailWebService()
    webapp.checkLogin = CheckLoginWebService()
    cherrypy.tree.mount(webapp, '/', config=conf)
    cherrypy.config.update({
        'tools.staticdir.debug': True,
        'log.screen': True,
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 8080,
        'tools.sessions.on': True,
        'tools.encode.on': True,
        'tools.encode.encoding': 'utf-8'
    })
    cherrypy.engine.start()